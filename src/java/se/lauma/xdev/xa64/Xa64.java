/*
 *  Xa64.java - Main class of the xa64 assembler
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64;

//{{{ Imports
import java.io.*;
import java.util.*;
import org.apache.commons.cli.*;
import se.lauma.xdev.xa64.instructions.*;
//}}}

/**
 *  The main class of the xa64 assembler
 *
 * @author     Andreas Lofwenmark
 * @created    February 20, 2005
 */
public class Xa64
{
  //{{{ Private objects
  private static String filename = null;
  private static boolean showInsList = false;
  private static boolean showSymbolTable = false;
  private static boolean showAst = false;
  private static boolean bare = false;
  private static boolean debug = false;
  private static Vector includeFiles = new Vector();
  private static Vector includeDirs = new Vector();
  private static String path = "";
  private static Options options = null;
  //}}}

  //{{{ parseArgs
  /**
   *  Parse the command line arguments
   *
   * @param  args  String array containing the arguments
   */
  private static void parseArgs( String[] args )
  {
    HelpFormatter formatter = new HelpFormatter();

    try {
      CommandLineParser parser = new PosixParser();
      CommandLine cmd = parser.parse( options, args );

      String[] cmd_args = cmd.getArgs();

      if( cmd_args.length == 0 ) {
        ErrorLog.fatalNoExit( "No input file\n" );
        formatter.printHelp( "xa64 [OPTION] ... FILE", options );
        System.exit( 1 );
      }
      else {
        filename = cmd_args[0];
        if( cmd_args.length > 1 ) {
          ErrorLog.warning( "Too many input files, using " + filename );
        }
      }

      if( cmd.hasOption( "b" ) ) {
        bare = true;
      }

      if( cmd.hasOption( "v" ) ) {
        ErrorLog.setVerbose( true );
      }

      if( cmd.hasOption( "s" ) ) {
        showSymbolTable = true;
      }

      if( cmd.hasOption( "l" ) ) {
        showInsList = true;
      }

      if( cmd.hasOption( "a" ) ) {
        showAst = true;
      }

      if( cmd.hasOption( "d" ) ) {
        debug = true;
      }

      if( cmd.hasOption( "I" ) ) {
        String includes = cmd.getOptionValue( "I" );

        if( includes != null ) {
          String[] dirs = includes.split( File.pathSeparator );

          for( int i = 0; i < dirs.length; i++ ) {
            String endChar = "";

            if( !dirs[i].endsWith( File.separator ) ) {
              endChar = File.separator;
            }
            includeDirs.add( dirs[i] + endChar );
          }

        }
        else {
          ErrorLog.fatalNoExit( "Missing include path argument for -I\n" );
          formatter.printHelp( "xa64 [OPTION] ... FILE", options );
          System.exit( 1 );
        }

      }
    }
    catch( ParseException e ) {
      ErrorLog.fatalNoExit( e.getMessage() + "\n" );
      formatter.printHelp( "xa64 [OPTION] ... FILE", options );
      System.exit( 1 );
    }

  }
  //}}}

  //{{{ setupOptions
  /**
   *  Add all valid command line arguments to the <code>options</code> object
   */
  private static void setupOptions()
  {
    options = new Options();
    options.addOption( "v", "verbose", false, "Enable verbose mode" );
    options.addOption( "l", "showins", false, "Print instruction list" );
    options.addOption( "s", "showsym", false, "Print symbol table" );
    options.addOption( "a", "showast", false, "Print AST" );
    options.addOption( "b", "bare", false, "Write bare binary, no start address in first two bytes" );
    options.addOption( "d", "debug", false, "Use debug version of parser" );
    options.addOption( OptionBuilder.withArgName( "include path" )
      .hasArg()
      .withDescription( "Include paths separated with " + File.pathSeparator )
      .create( "I" ) );
  }
  //}}}

  //{{{ main()
  /**
   *  The main method of the xa64 application. This should never be invoked
   *  directly
   *
   * @param  args  The command line arguments
   */
  public static void main( String args[] )
  {
    System.out.println( "xa64 v" + Version.version + " (build " + Version.build + ")" );

    String userDir = System.getProperty( "user.dir" ) + File.separator;

    includeDirs.add( userDir );

    setupOptions();
    parseArgs( args );

    File f = new File( filename );

    if( f.getParent() != null ) {
      path = f.getParent() + File.separator;
      if( !userDir.equals( path ) ) {
        includeDirs.add( 0, path );
      }
    }

    ErrorLog.startFile( f.getName() );
    compileInclude( f.getName() );

    AsmFile asmFile = AsmFile.getAsmFile();

    if( ErrorLog.getNumErrors() == 0 ) {
      ErrorLog.message( "End PC = $" +
        Integer.toHexString( asmFile.getPC() ) );
      ErrorLog.message( "Pass 2: Generating output" );

      Vector program = asmFile.asm( bare );

      if( showInsList ) {
        System.out.println();
        System.out.print( asmFile.insListToString() );
      }

      if( showSymbolTable ) {
        System.out.println();
        System.out.print( asmFile.symbolTableToString() );
      }

      if( showAst ) {
        System.out.println();
        System.out.print( asmFile.astToString() );
      }

      if( ErrorLog.getNumErrors() == 0 ) {
        File file;

        if( !bare ) {
          file = new File( filename.substring( 0, filename.lastIndexOf( '.' ) ) + ".prg" );
        }
        else {
          file = new File( filename.substring( 0, filename.lastIndexOf( '.' ) ) + ".bin" );
        }
        try {
          file.createNewFile();

          FileOutputStream fos = new FileOutputStream( file );

          for( Enumeration en = program.elements(); en.hasMoreElements();  ) {
            Integer code = (Integer)en.nextElement();

            fos.write( code.intValue() );
          }
          fos.close();
        }
        catch( Exception e ) {
          ErrorLog.fatal( e.getMessage() );
        }
      }
    }

    if( showInsList || showSymbolTable ) {
      System.out.println();
    }
    System.out.println( ErrorLog.getNumErrors() + " errors and " +
      ErrorLog.getNumWarnings() + " warnings found." );
    System.out.println();
  }
  //}}}

  //{{{ compileInclude()
  /**
   *  Performs the compilation of files. Both the main and any include files.
   *
   * @param  filename  Name of the file to compile
   */
  public static void compileInclude( String filename )
  {
    FileInputStream fileInput = null;

    if( includeFiles.contains( filename ) ) {
      ErrorLog.fatal( "Circular dependency of include files, " + filename );
    }

    try {
      fileInput = new FileInputStream( path + filename );
    }
    catch( FileNotFoundException e ) {
      ErrorLog.fatal( path + filename + " was not found" );
    }

    File f = new File( path + filename );
    String simpleName = f.getName();

    ErrorLog.startFile( simpleName );
    ErrorLog.message( "Compiling" );

    includeFiles.add( simpleName );

    Lexer scanner = new Lexer( fileInput );
    Parser parser = new Parser( filename, scanner );

    ErrorLog.message( "Pass 1: Parsing" );
    try {
      if( !debug ) {
        parser.parse();
      }
      else {
        parser.debug_parse();
      }
    }
    catch( Exception e ) {
      e.printStackTrace();
    }
    finally {
      ErrorLog.endFile();
    }

    try {
      fileInput.close();
    }
    catch( Exception e ) {
    }
  }
  //}}}

  //{{{ getIncludeName()
  /**
   *  Looks for a file in the available include directories
   *
   * @param  filename  Name of the file to look for
   * @return           The full name of the file or null if not found
   */
  public static String getIncludeName( String filename )
  {
    String retStr = null;
    File f = null;

    for( Enumeration en = includeDirs.elements(); en.hasMoreElements();  ) {
      String dir = (String)en.nextElement();

      f = new File( dir + filename );

      if( f.exists() ) {
        retStr = dir + filename;
        break;
      }
    }
    return retStr;
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

