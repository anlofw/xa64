package se.lauma.xdev.xa64;

import java.io.InputStream;
import java_cup.runtime.Symbol;

%%

%class Lexer
%unicode
%cupsym Sym
%cup
%line
%column

%{
  public Lexer lexer;
  public Object lastToken;

  public int getLine() 
  { 
    return yyline+1; 
  }
  
  public int getColumn()
  {
    return yycolumn+1;
  }
  
/*
  public static void main(String[] args) 
  {
    try {
      Lexer lexer = new Lexer(System.in);
      while (true) { 
        Symbol s = lexer.next_token();
	if (s.sym == Sym.EOF) {
          System.out.println("EOF");
  	  break;
        }
        System.out.println("Symbol:" + s + ":" + s.value);
      }
    } 
    catch (Exception e) { 
      e.printStackTrace(); 
    }
  }
*/  
  private Symbol symbol( int type ) 
  {
    lastToken = yytext();
    return new Symbol( type, yyline, yycolumn );
  }

  private Symbol symbol( int type, Object value )
  {
    lastToken = yytext();
    return new Symbol( type, yyline, yycolumn, value );
  }

%} 

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment = ";" {InputCharacter}* {LineTerminator}?

DecIntegerLiteral = 0 | [1-9][0-9]*
BinIntegerLiteral = "%" [01]+
HexIntegerLiteral = "$" [a-fA-F0-9]+

Identifier = [a-zA-Z_@][a-zA-Z0-9_]* 

%states SPRT, BYTE
%%

[pP][cC]  { return symbol( Sym.PC, yytext() ); }
[vV][cC]  { return symbol( Sym.VC, yytext() ); }

"subroutine"  { return symbol( Sym.SUB ); }

"INCLUDE" { return symbol( Sym.INCLUDE ); }
"INCBIN"  { return symbol( Sym.INCBIN ); }
"ALIGN"   { return symbol( Sym.ALIGN ); }

","   { return symbol( Sym.COMMA ); }
"x"   { return symbol( Sym.X ); }
"y"   { return symbol( Sym.Y ); }
"a"   { return symbol( Sym.A ); }
"zp"  { return symbol( Sym.ZP ); }
"ab"  { return symbol( Sym.AB ); }

".byte"   { yybegin(BYTE); return symbol( Sym.BYTE, yytext() ); }
".word"   { return symbol( Sym.WORD, yytext() ); }
".sprt"   { yybegin(SPRT); return symbol( Sym.SPRT, yytext() ); }
".text"   { return symbol( Sym.PETSCII, yytext() ); }
".ansi"   { return symbol( Sym.ANSI, yytext() ); }
".screen" { return symbol( Sym.SCREEN, yytext() ); }

"+"  { return symbol( Sym.PLUS ); }
"-"  { return symbol( Sym.MINUS ); }
"*"  { return symbol( Sym.MULT ); }
"/"  { return symbol( Sym.DIV ); }
"!"  { return symbol( Sym.NOT ); }
"<"  { return symbol( Sym.LT ); }
">"  { return symbol( Sym.GT ); }
"("  { return symbol( Sym.LPAR ); }
")"  { return symbol( Sym.RPAR ); }
"["  { return symbol( Sym.LBRACK ); }
"]"  { return symbol( Sym.RBRACK ); }
"="  { return symbol( Sym.EQ ); }
"#"  { return symbol( Sym.HASH ); }
"."  { return symbol( Sym.DOT ); }

{Identifier}    { return symbol( Sym.IDENTIFIER, yytext() ); }
{Identifier}:   { return symbol( Sym.LABEL, yytext().substring(0,yytext().length()-1) ); }
.{Identifier}:  { return symbol( Sym.LOCAL_LABEL, yytext().substring(0,yytext().length()-1) ); }

{DecIntegerLiteral}  { return symbol( Sym.INTEGER, Integer.valueOf( yytext() ) ); }
{HexIntegerLiteral}  { return symbol( Sym.INTEGER, Integer.valueOf( yytext().substring(1), 16 ) ); }

<SPRT> {BinIntegerLiteral}  { yybegin(YYINITIAL); return symbol( Sym.SPRT_STRING, yytext().substring(1) ); }
<YYINITIAL, BYTE> {BinIntegerLiteral}  { return symbol( Sym.INTEGER, Integer.valueOf( yytext().substring(1), 2 ) ); }

\"{InputCharacter}*\" { return new Symbol(Sym.STRING,yychar,yychar+yytext().length(), yytext().substring(1,yytext().length()-1)); }

{Comment} { yybegin(YYINITIAL); return new Symbol(Sym.NEWLINE,yychar,yychar+1); }

(\r\n) { yybegin(YYINITIAL); return new Symbol(Sym.NEWLINE,yychar,yychar+2); }
([\r\n\f]) { yybegin(YYINITIAL); return new Symbol(Sym.NEWLINE,yychar,yychar+1); }

{WhiteSpace} { /* ignore */ }

. { return symbol(Sym.ILLEGAL_STRING,yytext());}

