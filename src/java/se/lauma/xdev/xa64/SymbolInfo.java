/*
 *  SymbolInfo.java - Handles a symbol table entry
 *
 *  Copyright (C) 2005-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64;

/**
 *  Handles a symbol table entry
 *
 * @author     andreas
 * @created    March 28, 2005
 */
public class SymbolInfo
{
  //{{{ Private objects
  private int instructionIndex = -1;
  private int value = -1;
  private int line = -1;
  private int pcAddress = -1;
  //}}}

  //{{{ Constructor
  /**
   *  Constructor for the SymbolInfo object
   *
   * @param  index  Description of the Parameter
   * @param  line   Description of the Parameter
   * @param  pc     Description of the Parameter
   * @param  value  Description of the Parameter
   */
  public SymbolInfo( int pc, int value, int index, int line )
  {
    pcAddress = pc;
    instructionIndex = index;
    this.line = line;
    this.value = value;
  }
  //}}}

  //{{{ Constructor
  /**
   *  Constructor for the SymbolInfo object
   *
   * @param  value  Description of the Parameter
   * @param  index  Description of the Parameter
   * @param  line   Description of the Parameter
   */
  public SymbolInfo( int value, int index, int line )
  {
    instructionIndex = index;
    this.line = line;
    this.value = value;
    pcAddress = -1;
  }
  //}}}

  //{{{ Constructor
  /**
   *  Constructor for the SymbolInfo object
   *
   * @param  line   Description of the Parameter
   * @param  value  Description of the Parameter
   */
  public SymbolInfo( int value, int line )
  {
    this.value = value;
    this.line = line;
    instructionIndex = -1;
    pcAddress = -1;
  }
  //}}}

  //{{{ setIndex()
  /**
   *  Sets the index attribute of the SymbolInfo object
   *
   * @param  index  The new index value
   */
  public void setIndex( int index )
  {
    instructionIndex = index;
  }
  //}}}

  //{{{ getLine()
  /**
   *  Gets the line attribute of the SymbolInfo object
   *
   * @return    The line value
   */
  public int getLine()
  {
    return line;
  }
  //}}}

  //{{{ getValue()
  /**
   *  Gets the value attribute of the SymbolInfo object
   *
   * @return    The value value
   */
  public int getValue()
  {
    return value;
  }
  //}}}

  //{{{ getPcAddress()
  /**
   *  Gets the pcAddress attribute of the SymbolInfo object
   *
   * @return    The pcAddress value
   */
  public int getPcAddress()
  {
    return pcAddress;
  }
  //}}}

  //{{{ getIndex()
  /**
   *  Gets the index attribute of the SymbolInfo object
   *
   * @return    The index value
   */
  public int getIndex()
  {
    return instructionIndex;
  }
  //}}}

  //{{{ toString()
  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  public String toString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "value = $" );
    strBuf.append( Integer.toHexString( value ).toUpperCase() );
    strBuf.append( " (" );
    strBuf.append( value );
    strBuf.append( ")" );
    strBuf.append( ", line = " );
    strBuf.append( line );
    if( instructionIndex != -1 ) {
      strBuf.append( ", instruction index = " );
      strBuf.append( instructionIndex );
    }

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

