/*
 *  AsmFile.java - Handles the resulting object code
 *
 *  Copyright (C) 2005-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64;

//{{{ Imports
import java.util.*;
import se.lauma.xdev.xa64.instructions.*;
//}}}

/**
 *  Handles the resulting object code
 *
 * @author     Andreas Lofwenmark
 * @created    February 27, 2005
 */
public class AsmFile
{
  //{{{ Private objects
  private int startPC = 0x2000;
  private int currentPC = startPC;
  private int currentVC = startPC;
  private boolean startPCSet = false;
  private Vector instructions = new Vector();
  private Hashtable symbolTable = new Hashtable();
  private static AsmFile theInstance = null;
  //}}}

  //{{{ getAsmFile()
  /**
   *  Gets the singleton AsmFile object.
   *
   * @return    The AsmFile instance
   */
  public static AsmFile getAsmFile()
  {
    if( theInstance == null ) {
      theInstance = new AsmFile();
    }
    return theInstance;
  }
  //}}}

  //{{{ Constructor
  /**
   *  Constructor for the AsmFile object.
   */
  private AsmFile()
  {
  }
  //}}}

  //{{{ getSymbolTable()
  /**
   *  Gets the symbol table.
   *
   * @return    The symbol table
   */
  public Hashtable getSymbolTable()
  {
    return symbolTable;
  }
  //}}}

  //{{{ addLabel()
  /**
   *  Adds a label to the symbol table. The symbol table entry contains links to
   *  the source code line, the program counter, the virtual program counter and
   *  instruction list index.
   *
   * @param  label      The name of the label
   * @param  line       The source code line where the label was found
   * @param  pcAddress  The program counter value the label corresponds to
   */
  public void addLabel( String label, int line, int pcAddress )
  {
    if( OpIns.opcMap.containsKey( label ) ) {
      ErrorLog.error( "line " + line + ": " + label
         + " is a reserved word and cannot be used as a label." );
    }
    else if( symbolTable.containsKey( label ) ) {
      int defLine = ( (SymbolInfo)symbolTable.get( label ) ).getLine();

      ErrorLog.error( "line " + line + ": " + label + " already defined on line " + defLine + "." );
    }
    else {
      int index = instructions.size();
      int vcAddress = ( (Ins)instructions.elementAt( index - 1 ) ).getAddress();
      SymbolInfo labelInfo = new SymbolInfo( pcAddress, vcAddress, index, line );

      symbolTable.put( label, labelInfo );
    }
  }
  //}}}

  //{{{ addDefine()
  /**
   *  Adds a define to the symbol table. The symbol table entry contains links
   *  to the source code line and the value of the define.
   *
   * @param  line  The source code line where the define was found
   * @param  def   The name of the define
   * @param  val   The value of the define
   */
  public void addDefine( int line, String def, Expression val )
  {
    if( OpIns.opcMap.containsKey( def ) ) {
      ErrorLog.error( "line " + line + ": " + def
         + " is a reserved word and cannot be used as a define." );
    }
    else if( symbolTable.containsKey( def ) ) {
      int defLine = ( (SymbolInfo)symbolTable.get( def ) ).getLine();

      ErrorLog.error( "line " + line + ": " + def + " already defined on line " + defLine + "." );
    }
    else {
      try {
        int value = val.getValue();
        SymbolInfo defineInfo = new SymbolInfo( value, line );

        symbolTable.put( def, defineInfo );
      }
      catch( NumberFormatException exception ) {
        String msg = exception.getMessage();
        int index = msg.indexOf( '.' );
        String label = msg;
        String scope = "";

        if( index > 0 ) {
          label = msg.substring( index );
          scope = " in the scope of " + msg.substring( 0, index );
        }
        ErrorLog.error( "line " + line + ": "
           + label + " is undefined" + scope );
      }
    }
  }
  //}}}

  //{{{ addIns()
  /**
   *  Adds an instruction to the instruction list.
   *
   * @param  i  The instruction to appended to the list
   */
  public void addIns( Ins i )
  {
    i.setAddress( currentVC );
    instructions.add( i );
    currentPC += i.getNumBytes();
    currentVC += i.getNumBytes();
  }

  /**
   *  Adds an instruction to the instruction list.
   *
   * @param  i          The instruction to be appended to the list
   * @param  label      The label associated with the instruction
   * @param  labelLine  The source code line where the label was found
   */
  public void addIns( Ins i, String label, int labelLine )
  {
    i.setLabel( label );
    i.setAddress( currentVC );
    instructions.add( i );
    addLabel( label, labelLine, currentPC );
    currentPC += i.getNumBytes();
    currentVC += i.getNumBytes();
  }
  //}}}

  //{{{ asm()
  /**
   *  Performs pass 2 of the assembly.
   *
   * @param  bare  True if a bare binary file is to be generated, i.e. no load
   *      address in the first two bytes.
   * @return       The resulting object code
   */
  public Vector asm( boolean bare )
  {
    Vector program = new Vector();

    if( !startPCSet ) {
      ErrorLog.warning( "No PC assignment, using address $" + Integer.toHexString( startPC ) + " as start" );
    }

    if( !bare ) {
      program.add( new Integer( startPC & 255 ) );
      program.add( new Integer( startPC / 256 ) );
    }
    for( Enumeration en = instructions.elements(); en.hasMoreElements();  ) {
      Ins ins = (Ins)en.nextElement();
      Vector code = ins.generateCode();

      if( code != null ) {
        program.addAll( code );
      }
    }
    return program;
  }
  //}}}

  //{{{ getPC()
  /**
   *  Gets the program counter value.
   *
   * @return    The program counter value
   */
  public int getPC()
  {
    return currentPC;
  }
  //}}}

  //{{{ setPC()
  /**
   *  Sets the program counter value.
   *
   * @param  line  The source code line where the assignment was found
   * @param  pc    The new program counter value
   */
  public void setPC( int line, int pc )
  {
    if( !startPCSet ) {
      if( currentPC > startPC ) {
        ErrorLog.warning( "line " + line + ": Ignoring start PC assignment, using $" +
          Integer.toHexString( startPC ) );
      }
      else {
        startPC = pc;
        currentPC = pc;
        currentVC = pc;
      }
      startPCSet = true;
      ErrorLog.message( "line " + line + ": PC = $" + Integer.toHexString( currentPC ) );
    }
    else if( currentPC >= pc ) {
      ErrorLog.error( "line " + line + ": New PC must be greater than current PC" );
    }
    else {
      if( instructions.size() != 0 ) {
        FillIns ins = new FillIns( pc - currentPC );

        addIns( ins );
      }
      else {
        currentPC = pc;
        currentVC = pc;
      }
      ErrorLog.message( "line " + line + ": PC = $" + Integer.toHexString( currentPC ) );
    }
  }
  //}}}

  //{{{ getVC()
  /**
   *  Gets the virtual program counter value.
   *
   * @return    The virtual program counter value
   */
  public int getVC()
  {
    return currentVC;
  }
  //}}}

  //{{{ setVC()
  /**
   *  Sets the virtual program counter value.
   *
   * @param  line  The source code line where the assignment was found
   * @param  vc    The new virtual prgram counter value
   */
  public void setVC( int line, int vc )
  {
    currentVC = vc;
    ErrorLog.message( "line " + line + ": VC = $" + Integer.toHexString( currentVC ) );
  }
  //}}}

  //{{{ symbolTableToString()
  /**
   *  Transforms the symbol table into a string.
   *
   * @return    The resulting string representation of the symbol table
   */
  public String symbolTableToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "Symbol Table\n" );
    strBuf.append( "------------\n" );
    for( Enumeration en = symbolTable.keys(); en.hasMoreElements();  ) {
      String key = (String)en.nextElement();
      SymbolInfo info = (SymbolInfo)symbolTable.get( key );

      strBuf.append( key + ": " + info + "\n" );
    }
    return strBuf.toString();
  }
  //}}}

  //{{{ insListToString
  /**
   *  Transforms the instruction list into a string.
   *
   * @return    The string representation of the instruction list
   */
  public String insListToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "Instruction List\n" );
    strBuf.append( "----------------\n" );
    for( Enumeration en = instructions.elements(); en.hasMoreElements();  ) {
      Ins ins = (Ins)en.nextElement();

      strBuf.append( ins.toString() + "\n" );
    }
    return strBuf.toString();
  }
  //}}}

  //{{{ astToString()
  /**
   *  Transforms the Abstract Syntax Tree (AST) into a string.
   *
   * @return    The string representation of the AST
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "AST\n" );
    strBuf.append( "---\n" );
    for( Enumeration en = instructions.elements(); en.hasMoreElements();  ) {
      Ins ins = (Ins)en.nextElement();

      strBuf.append( ins.astToString() + "\n" );
    }

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

