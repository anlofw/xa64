/*
 *  Version.java - Contains version and build information
 *
 *  Copyright (C) 2005-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64;

/**
 *  Contains version and build information
 *
 * @author     andreas
 * @created    July 17, 2005
 */
public class Version
{
  /**
   *  The application version, this will be updated during compilation
   */
  public final static String version = "@VERSION@";

  /**
   *  The application build number, this will be updated during compilation
   */
  public final static String build = "@BUILD@";
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

