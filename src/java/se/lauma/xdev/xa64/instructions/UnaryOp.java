//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/UnaryOp.java $
// $LastChangedRevision: 8 $
// $LastChangedDate: 2006-10-08 02:57:30 +0200 (Sun, 08 Oct 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 * The class UnaryOp is used as base class for all unary operatios
 *
 * @author  Andreas Lofwenmark
 */

public abstract class UnaryOp extends Expression
{
    protected Expression right;

}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
