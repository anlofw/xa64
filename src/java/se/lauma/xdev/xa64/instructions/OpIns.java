/*
 *  OpIns.java - Represents a 6510 instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.io.*;
import java.util.*;
import se.lauma.xdev.xa64.*;
//}}}

/**
 *  Represents a 6510 instruction
 *
 * @author     Andreas Lofwenmark
 * @created    February 23, 2005
 */
public class OpIns extends Ins
{
  //{{{ Private objects
  private int opCode;
  private String opCodeString;
  private String modeString;
  private Expression value;
  private boolean isRelative;

  /**
   *  Contains mappings between names and values of instructions
   */
  public static Map opcMap = new HashMap();
  private static Map immMap = new HashMap();
  private static Map zpMap = new HashMap();
  private static Map zpxMap = new HashMap();
  private static Map zpyMap = new HashMap();
  private static Map absMap = new HashMap();
  private static Map absxMap = new HashMap();
  private static Map absyMap = new HashMap();
  private static Map indirxMap = new HashMap();
  private static Map indiryMap = new HashMap();
  private static Map impMap = new HashMap();
  private static Map relMap = new HashMap();
  private static Map indMap = new HashMap();
  //}}}

  //{{{ Static initializer
  static {
    try {
      loadMap( opcMap, "se.lauma.xdev.xa64.instructions.opcodes" );
      loadMap( immMap, "se.lauma.xdev.xa64.instructions.imm" );
      loadMap( zpMap, "se.lauma.xdev.xa64.instructions.zp" );
      loadMap( zpxMap, "se.lauma.xdev.xa64.instructions.zpx" );
      loadMap( zpyMap, "se.lauma.xdev.xa64.instructions.zpy" );
      loadMap( absMap, "se.lauma.xdev.xa64.instructions.abs" );
      loadMap( absxMap, "se.lauma.xdev.xa64.instructions.absx" );
      loadMap( absyMap, "se.lauma.xdev.xa64.instructions.absy" );
      loadMap( indirxMap, "se.lauma.xdev.xa64.instructions.indirx" );
      loadMap( indiryMap, "se.lauma.xdev.xa64.instructions.indiry" );
      loadMap( impMap, "se.lauma.xdev.xa64.instructions.imp" );
      loadMap( relMap, "se.lauma.xdev.xa64.instructions.rel" );

      indMap.put( "jmp", new Integer( 0x6c ) );
    }
    catch( IOException e ) {
      ErrorLog.fatal( "Could not find/open all config files." );
    }
  }
  //}}}

  //{{{ loadMap()
  /**
   *  Loads a property file into a Map.
   *
   * @param  map              Map to be filled
   * @param  fileName         Name of the property file to load
   * @exception  IOException  File I/O exception
   */
  private static void loadMap( Map map, String fileName ) throws
    IOException
  {
    map.clear();

    PropertyResourceBundle rb = (PropertyResourceBundle)
      ResourceBundle.getBundle( fileName );
    Enumeration e = rb.getKeys();

    while( e.hasMoreElements() ) {
      String k = (String)e.nextElement();
      String v = (String)rb.handleGetObject( k );
      Integer intVal = Integer.decode( v );

      map.put( k.toLowerCase(), intVal );
    }
  }
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the OpIns object.
   */
  private OpIns()
  {
    super();
    opCode = -1;
    value = null;
    opCodeString = "UNKNOWN";
    modeString = "unknown";
    isRelative = false;
  }
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the OpIns object.
   *
   * @param  line  Source code line associated with this instruction
   */
  public OpIns( int line )
  {
    super( line );
    opCode = -1;
    value = null;
    opCodeString = "UNKNOWN";
    modeString = "unknown";
    isRelative = false;
  }
  //}}}

  //{{{ setOpCode()
  /**
   *  Sets the opcode value of this instruction.
   *
   * @param  op    The instruction name
   * @param  map   The map to be used to find the opcode value
   * @param  mode  The addressing mode for error output
   */
  private void setOpCode( String op, Map map, String mode )
  {
    Integer baseOp = (Integer)opcMap.get( op );
    Integer addressMode = (Integer)map.get( op );

    if( baseOp == null ) {
      ErrorLog.error( "line " + line + ": Unknown instruction " + op );
    }
    else if( addressMode == null ) {
      ErrorLog.error( "line " + line + ": " + mode + " addressing mode illegal"
         + ": " + op.toUpperCase() );
    }
    else {
      opCode = addressMode.intValue();
      modeString = mode;
      opCodeString = op.toUpperCase();
//      System.out.println( "Line " + line + ": " + modeString + " " + opCodeString + " = 0x" + Integer.toHexString( opCode ) );
    }
  }
  //}}}

  //{{{ encImm()
  /**
   *  Sets this object to an immediate instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encImm( String op, Expression e )
  {
    setOpCode( op, immMap, "Immediate" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encAbsZpRel()
  /**
   *  Sets this object to an absolute, zero page or relative instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbsZpRel( String op, Expression e )
  {
    if( relMap.containsKey( op ) ) {
      encRel( op, e );
    }
    else {
      int val = -1;
      boolean valOk = true;

      try {
        val = e.getValue();
      }
      catch( NumberFormatException exception ) {
        valOk = false;
      }
      if( !valOk || val > 0xff || ( absMap.containsKey( op ) && !zpMap.containsKey( op ) ) ) {
        setOpCode( op, absMap, "Absolute" );
        numBytes = 3;
      }
      else {
        setOpCode( op, zpMap, "Zero Page" );
        numBytes = 2;
      }
      value = e;
    }
  }
  //}}}

  //{{{ encAbs()
  /**
   *  Sets this object to an absolute instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbs( String op, Expression e )
  {
    setOpCode( op, absMap, "Absolute" );
    value = e;
    numBytes = 3;
  }
  //}}}

  //{{{ encZp()
  /**
   *  Sets this object to a zero page instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encZp( String op, Expression e )
  {
    setOpCode( op, zpMap, "Zero Page" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encAbsZpX()
  /**
   *  Sets this object to an absolute,x or zero page,x instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbsZpX( String op, Expression e )
  {
    int val = -1;
    boolean valOk = true;

    try {
      val = e.getValue();
    }
    catch( NumberFormatException exception ) {
      valOk = false;
    }
    if( !valOk || val > 0xff || ( absxMap.containsKey( op ) && !zpxMap.containsKey( op ) ) ) {
      setOpCode( op, absxMap, "Absolute, X" );
      numBytes = 3;
    }
    else {
      setOpCode( op, zpxMap, "Zero Page, X" );
      numBytes = 2;
    }
    value = e;
  }
  //}}}

  //{{{ encAbsX()
  /**
   *  Sets this object to an absolute,x instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbsX( String op, Expression e )
  {
    setOpCode( op, absxMap, "Absolute, X" );
    value = e;
    numBytes = 3;
  }
  //}}}

  //{{{ encZpX()
  /**
   *  Sets this object to an zero page,x instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encZpX( String op, Expression e )
  {
    setOpCode( op, zpxMap, "Zero Page, X" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encAbsZpY()
  /**
   *  Sets this to an absolute,y or zero page,y instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbsZpY( String op, Expression e )
  {
    int val = -1;
    boolean valOk = true;

    try {
      val = e.getValue();
    }
    catch( NumberFormatException exception ) {
      valOk = false;
    }
    if( !valOk || e.getValue() > 0xff || ( absyMap.containsKey( op ) && !zpyMap.containsKey( op ) ) ) {
      setOpCode( op, absyMap, "Absolute, Y" );
      numBytes = 3;
    }
    else {
      setOpCode( op, zpyMap, "Zero Page, Y" );
      numBytes = 2;
    }
    value = e;
  }
  //}}}

  //{{{ encAbsY()
  /**
   *  Sets this object to an absolute,y instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encAbsY( String op, Expression e )
  {
    setOpCode( op, absyMap, "Absolute, Y" );
    value = e;
    numBytes = 3;
  }
  //}}}

  //{{{ encZpY()
  /**
   *  Sets this object to a zero page,y instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encZpY( String op, Expression e )
  {
    setOpCode( op, zpyMap, "Zero Page, Y" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encIndirX()
  /**
   *  Sets this object to an (indirect,x) instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encIndirX( String op, Expression e )
  {
    // TODO: Only Zp
    setOpCode( op, indirxMap, "(Indirect, X)" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encIndirY()
  /**
   *  Sets this object to an (indirect),y instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encIndirY( String op, Expression e )
  {
    setOpCode( op, indiryMap, "(Indirect), Y" );
    value = e;
    numBytes = 2;
  }
  //}}}

  //{{{ encImp()
  /**
   *  Sets this object to an implied instruction.
   *
   * @param  op  The instruction name
   */
  public void encImp( String op )
  {
    setOpCode( op, impMap, "Implied" );
    numBytes = 1;
  }
  //}}}

  //{{{ encRel()
  /**
   *  Sets this object to a relative instruction.
   *
   * @param  op  The instruction name
   */
  public void encRel( String op )
  {
    setOpCode( op, relMap, "Relative" );
    numBytes = 2;
  }
  //}}}

  //{{{ encRel()
  /**
   *  Sets this object to a relative instruction.
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encRel( String op, Expression e )
  {
    setOpCode( op, relMap, "Relative" );
    value = e;
    isRelative = true;
    numBytes = 2;
  }
  //}}}

  //{{{ encInd()
  /**
   *  Sets this to an (indirect) instruction
   *
   * @param  op  The instruction name
   * @param  e   The operand value
   */
  public void encInd( String op, Expression e )
  {
    setOpCode( op, indMap, "Indirect" );
    value = e;
    numBytes = 3;
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    Vector code = new Vector();

    code.add( new Integer( opCode ) );

    try {
      if( numBytes == 2 ) {
        if( isRelative ) {
          // TODO: Handle out of range
          code.add( new Integer( ( value.getValue() - ( address + 2 ) ) & 255 ) );
        }
        else {
          code.add( new Integer( value.getValue() & 255 ) );
        }
      }
      else if( numBytes == 3 ) {
        code.add( new Integer( value.getValue() & 255 ) );
        code.add( new Integer( value.getValue() / 256 ) );
      }
    }
    catch( NumberFormatException exception ) {
      String msg = exception.getMessage();
      int index = msg.indexOf( '.' );
      String label = msg;
      String scope = "";

      if( index > 0 ) {
        label = msg.substring( index );
        scope = " in the scope of " + msg.substring( 0, index );
      }
      ErrorLog.error( "line " + line + ": "
         + label + " is undefined" + scope );

    }

    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this OpIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "OpIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes + ", " + modeString + " " + opCodeString + " = 0x" +
      Integer.toHexString( opCode );
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "OpIns\n" );
    if( value != null ) {
      strBuf.append( beginChild() );
    }
    else {
      strBuf.append( lastChild() );
    }
    strBuf.append( opCodeString + " ($" + Integer.toHexString( opCode ).toUpperCase() +
      ", " + modeString + ")" );
    if( value != null ) {
      strBuf.append( "\n" );
    }
    endChild();
    if( value != null ) {
      strBuf.append( lastChild() );
      strBuf.append( value );
      endChild();
    }

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

