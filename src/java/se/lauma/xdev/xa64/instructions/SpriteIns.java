/*
 *  SpriteIns.java - Represents a .sprt instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.io.*;
import java.util.*;
import se.lauma.xdev.xa64.*;
//}}}

/**
 *  Represents a .sprt instruction
 *
 * @author     Andreas Lofwenmark
 * @created    February 23, 2005
 */
public class SpriteIns extends Ins
{
  //{{{ Private objects
  private Vector value = new Vector();
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the SpriteIns object.
   */
  private SpriteIns()
  {
    super();
  }
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the SpriteIns object.
   *
   * @param  line  The source code line associated with this instruction
   */
  public SpriteIns( int line )
  {
    super( line );
  }
  //}}}

  //{{{ encString()
  /**
   *  Encodes the string of zeroes and ones as byte values.
   *
   * @param  sprtVal  The string of zeroes and ones
   */
  public void encString( String sprtVal )
  {
    if( sprtVal.length() != 24 ) {
      ErrorLog.error( "line " + line + ": " + sprtVal.length()
         + " bits in .sprt, must be 24" );
    }
    else {
      try {
        value.add( Integer.valueOf( sprtVal.substring( 0, 8 ), 2 ) );
        value.add( Integer.valueOf( sprtVal.substring( 8, 16 ), 2 ) );
        value.add( Integer.valueOf( sprtVal.substring( 16, 24 ), 2 ) );
        numBytes = 3;
      }
      catch( NumberFormatException exception ) {
        ErrorLog.error( "line " + line + ": " + exception.getMessage() );
      }
    }
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the .sprt instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    return value;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this SpriteIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "SpriteIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes;
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "WordIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "Size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

