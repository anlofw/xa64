/*
 *  Gt.java - Represents the low byte of an expression
 *
 *  Copyright (C) 2003-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 * The class Lt is used to represent <expression
 * it returns expression & 255
 *
 * @author  Andreas Lofwenmark
 */

public class Lt extends UnaryOp
{
  public Lt( Expression e) 
  {
    right = e;
  }
  
  public int getValue() 
  {
    return right.getValue() & 255;
  }
  
  public String toString()
  {
    String out = "";
    out += "Low byte (<) of\n";
    out += lastChild();
    out += right;
    endChild();
    
    return out;
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
