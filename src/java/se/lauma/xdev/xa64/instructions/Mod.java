//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/Mod.java $
// $LastChangedRevision: 8 $
// $LastChangedDate: 2006-10-08 02:57:30 +0200 (Sun, 08 Oct 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 * The class Mod is used to represent a ramainder operation
 *
 * @author  Andreas Lofwenmark
 */

public class Mod extends BinaryOp
{
  public Mod( Expression l, Expression r )
  {
    left = l;
    right = r;
  }

  public int getValue()
  {
    return left.getValue() % right.getValue();
  }

  public String toString()
  {
    return toString("Mod (%)");
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
