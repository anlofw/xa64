/*
 *  ByteIns.java - Represents a .byte instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.util.*;
import se.lauma.xdev.xa64.*;
//}}}

/**
 *  Represents a .byte instruction
 *
 * @author     Andreas Lofwenmark
 * @created    February 27, 2005
 */
public class ByteIns extends Ins
{
  //{{{ Private objects
  private Vector bytes = new Vector();
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the ByteIns object.
   *
   * @param  line  Source code line associated with this instruction
   */
  public ByteIns( int line )
  {
    super( line );
    numBytes = 0;
  }
  //}}}

  //{{{ add()
  /**
   *  Adds a new byte object.
   *
   * @param  b  The byte object
   */
  public void add( Expression b )
  {
    bytes.add( b );

    numBytes += 1;
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the .byte instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    Vector code = new Vector();

    try {
      for( int i = 0; i < bytes.size(); i++ ) {
        int val = ( (Expression)bytes.elementAt( i ) ).getValue();

        if( val >= -0xff && val <= 0xff ) {
          code.add( new Integer( val & 0xff ) );
        }
        else {
          ErrorLog.error( "line " + line +
            ": Byte at index " + ( i + 1 ) + " out of range" );
        }
      }
    }
    catch( NumberFormatException exception ) {
      String msg = exception.getMessage();
      int index = msg.indexOf( '.' );
      String label = msg;
      String scope = "";

      if( index > 0 ) {
        label = msg.substring( index );
        scope = " in the scope of " + msg.substring( 0, index );
      }
      ErrorLog.error( "line " + line + ": " +
        label + " is undefined" + scope );
      code = null;
    }

    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this ByteIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "ByteIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes;
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "ByteIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "Size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

