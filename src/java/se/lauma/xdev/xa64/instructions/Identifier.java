/*
 *  Identifier.java - Represents a label or define in an expression
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

import se.lauma.xdev.xa64.*;

/**
 *  Represents a label or define in an expression
 *
 * @author     Andreas Lofwenmark
 * @created    February 27, 2005
 */

public class Identifier extends Expression
{
  private String identifier;
  private boolean dotPC;

  /**
   *  Default constructor.
   */
  public Identifier()
  {
    identifier = null;
    dotPC = false;
  }

  /**
   *  Constructor for the Identifier object.
   *
   * @param  id  The name of the identifier
   */
  public Identifier( String id )
  {
    identifier = id;
    dotPC = false;
  }

  /**
   *  Constructor for the Identifier object.
   *
   * @param  id      The name of the identifier
   * @param  dotted  True if the identifiers PC address shall be returned
   */
  public Identifier( String id, boolean dotted )
  {
    identifier = id;
    dotPC = dotted;
  }

  /**
   *  Returns the value this identifier represent. The value is in the symbol
   *  table. If dotPC is true the PC address is returned instead of the value.
   *
   * @return                            The value
   * @exception  NumberFormatException  Exception thrown if identifier isn't
   *      found in the symbol table
   */
  public int getValue()
     throws NumberFormatException
  {
    int retVal;

    if( AsmFile.getAsmFile().getSymbolTable().containsKey( identifier ) ) {
      SymbolInfo info = (SymbolInfo)AsmFile.getAsmFile().getSymbolTable().get( identifier );

      if( dotPC ) {
        retVal = info.getPcAddress();
      }
      else {
        retVal = info.getValue();
      }
    }
    else {
      throw new NumberFormatException( identifier );
    }
    return retVal;
  }

  /**
   *  Prints information about this Identifier.
   *
   * @return    The string representation of the identifier
   */
  public String toString()
  {
    String out = "";

    out += "Identifier (" + identifier + ")";

    return out;
  }
}

