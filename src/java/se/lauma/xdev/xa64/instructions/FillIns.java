/*
 *  FillIns.java - Represents a gap in the output file filled with zeros
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.util.*;
//}}}

/**
 *  Represents a gap in the output file
 *
 * @author     andreas
 * @created    March 28, 2005
 */
public class FillIns extends Ins
{
  //{{{ Constructor()
  /**
   *  Constructor for the FillIns object
   *
   * @param  size  The size of the gap
   */
  public FillIns( int size )
  {
    numBytes = size;
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates size number of zeros for the gap.
   *
   * @return    The generated gap
   */
  public Vector generateCode()
  {
    Vector code = new Vector();

    for( int i = 0; i < numBytes; i++ ) {
      code.add( new Integer( 0 ) );
    }
    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this FillIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "FillIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes;
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "FillIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "Size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

