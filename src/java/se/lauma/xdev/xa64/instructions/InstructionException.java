
package se.lauma.xdev.xa64.instructions;

public class InstructionException extends Exception
{
  public InstructionException()
  {
    super();
  }

  public InstructionException(String s)
  {
    super( s );
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
