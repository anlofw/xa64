//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/Expression.java $
// $LastChangedRevision: 1 $
// $LastChangedDate: 2006-08-23 01:36:11 +0200 (Wed, 23 Aug 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

public abstract class Expression extends AstNode
{
  public Expression()
  {
  }
  
  public int getValue()  
  {
    return 0;
  }
  
  public String toString()
  {
    return "Expression";
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
