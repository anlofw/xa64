/*
 *  Ins.java - The base class for all instructions
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

import java.io.*;
import java.util.*;
import se.lauma.xdev.xa64.*;

/**
 *  Description of the Class
 *
 * @author     andreas
 * @created    February 23, 2005
 */
public abstract class Ins extends AstNode
{
  /**
   *  Description of the Field
   */
  protected int line;
  /**
   *  Description of the Field
   */
  protected int numBytes;
  /**
   *  Description of the Field
   */
  protected int address;

  /**
   *  Description of the Field
   */
//  protected int pcAddress;

  /**
   *  Description of the Field
   */
  protected String label;

  /**
   *  Constructor for the Ins object
   */
  protected Ins()
  {
    line = -1;
    numBytes = -1;
    address = -1;
//    pcAddress = -1;
    label = null;
  }

  /**
   *  Constructor for the Ins object
   *
   * @param  line  Description of the Parameter
   */
  public Ins( int line )
  {
    this.line = line;
    numBytes = -1;
    address = -1;
//    pcAddress = -1;
    label = null;
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  public Vector generateCode()
  {
    return null;
  }

  /**
   *  Gets the numBytes attribute of the Ins object
   *
   * @return    The numBytes value
   */
  public int getNumBytes()
  {
    return numBytes;
  }

  /**
   *  Gets the line attribute of the Ins object
   *
   * @return    The line value
   */
  public int getLine()
  {
    return line;
  }

  /**
   *  Sets the label attribute of the Ins object
   *
   * @param  label  The new label value
   */
  public void setLabel( String label )
  {
    this.label = label;
  }

  /**
   *  Sets the address attribute of the Ins object
   *
   * @param  address  The new address value
   */
  public void setAddress( int address )
  {
    this.address = address;
  }

  /**
   *  Gets the address attribute of the Ins object
   *
   * @return    The address value
   */
  public int getAddress()
  {
    return address;
  }

  public String astToString()
  {
    return "";
  }
  
//  public void setPcAddress( int address )
//  {
//    pcAddress = address;
//  }


//  public int getPcAddress()
//  {
//    return pcAddress;
//  }
}

