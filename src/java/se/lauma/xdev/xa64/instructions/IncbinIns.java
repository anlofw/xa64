/*
 *  IncbinIns.java - Represents an included binary file
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.io.*;
import java.util.*;
import se.lauma.xdev.xa64.*;
//}}}

/**
 *  Represents an included binary file
 *
 * @author     Andreas Lofwenmark
 * @created    August 13, 2005
 */
public class IncbinIns extends Ins
{
  //{{{ Private objects
  private String fileName = "";
  //}}}

  //{{{ Constructor
  /**
   *  Constructor for the IncbinIns object
   *
   * @param  line  Source code line associated with this instruction
   */
  public IncbinIns( int line )
  {
    super( line );
  }
  //}}}

  //{{{ setFile()
  /**
   *  Sets the filename of the file to include
   *
   * @param  file  The filename
   */
  public void setFile( String file )
  {
    fileName = file;

    File f = new File( fileName );

    if( f.getParent() == null ) {
      fileName = Xa64.getIncludeName( fileName );
    }
    else if( !f.exists() ) {
      fileName = null;
    }

    if( fileName == null ) {
      ErrorLog.error( "line " + line + ": " + file + " was not found" );
      numBytes = 0;
    }
    else {
      f = new File( fileName );
      ErrorLog.message( "line " + line + ": INCBIN file is " + fileName );
      numBytes = (int)f.length();
    }
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    FileInputStream fileInput = null;
    int value = -1;
    Vector code = new Vector();


    try {
      fileInput = new FileInputStream( fileName );
    }
    catch( FileNotFoundException e ) {
      ErrorLog.fatal( fileName + " was not found" );
    }

    try {
      while( ( value = fileInput.read() ) != -1 ) {
        code.add( new Integer( value ) );
      }
    }
    catch( IOException e ) {
      ErrorLog.fatal( e.getMessage() + "\n" );
    }

    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this IncbinIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "IncbinIns: address = $" );
    strBuf.append( Integer.toHexString( address ).toUpperCase() );
    strBuf.append( ", size = " );
    strBuf.append( numBytes );
    strBuf.append( ", file = " );
    strBuf.append( fileName );

    return strBuf.toString();
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "IncbinIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "File: " + fileName + ", size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

