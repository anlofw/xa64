/*
 *  IntegerConstant.java - Represents an integer constant
 *
 *  Copyright (C) 2003-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 *  Description of the Class
 *
 * @author     andreas
 * @created    14 September 2003
 */
public class IntegerConstant extends Expression
{
  //{{{ Private objects
  private int value;
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the IntegerConstant object
   */
  public IntegerConstant()
  {
    value = 0;
  }
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the IntegerConstant object
   *
   * @param  val  The value
   */
  public IntegerConstant( int val )
  {
    value = val;
  }
  //}}}

  //{{{ getValue()
  /**
   *  Returns the value
   *
   * @return    The value
   */
  public int getValue()
  {
    return value;
  }
  //}}}

  //{{{ toString
  /**
   *  Prints information about this expression.
   *
   * @return    The string representation of the expression
   */
  public String toString()
  {
    String out = "";

    out += "IntegerConstant ($" + Integer.toHexString( value ).toUpperCase() +
      ")";

    return out;
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

