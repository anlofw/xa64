//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/Mult.java $
// $LastChangedRevision: 8 $
// $LastChangedDate: 2006-10-08 02:57:30 +0200 (Sun, 08 Oct 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 * The class Mult is used to represent a multiplication
 *
 * @author  Andreas Lofwenmark
 */

public class Mult extends BinaryOp
{
  public Mult( Expression l, Expression r )
  {
    left = l;
    right = r;
  }

  public int getValue()
  {
    return left.getValue() * right.getValue();
  }

  public String toString()
  {
    return toString("Mult");
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
