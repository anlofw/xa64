//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/UnaryMinus.java $
// $LastChangedRevision: 8 $
// $LastChangedDate: 2006-10-08 02:57:30 +0200 (Sun, 08 Oct 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 * The class UnaryMinus is used to represent -expression
 * it returns -expression
 *
 * @author  Andreas Lofwenmark
 */

public class UnaryMinus extends UnaryOp
{
  public UnaryMinus( Expression e) 
  {
    right = e;
  }
  
  public int getValue() 
  {
    return -right.getValue();
  }

  public String toString()
  {
    String out = "";
    out += "Negative (2's complement) of\n";
    out += lastChild();
    out += right;
    endChild();
    
    return out;
  }

}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
