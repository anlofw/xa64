/*
 *  StringIns.java - Represents a .text, .ansi or a .screen instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.io.*;
import java.util.*;
//}}}

/**
 *  Represents a .text, .ansi or .screen instruction
 *
 * @author     Andreas Lofwenmark
 * @created    August 13, 2005
 */
public class StringIns extends Ins
{
  //{{{ Private objects
  private String textString = "";
  private String typeString = "";
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the StringIns object
   *
   * @param  line  Source code line associated with this instruction
   */
  public StringIns( int line )
  {
    super( line );
  }
  //}}}

  //{{{ encPETSCII()
  /**
   *  Encodes the string as PETSCII
   *
   * @param  text  The string to encode
   */
  public void encPETSCII( String text )
  {
    textString = asciiToPetscii( text );
    numBytes = textString.length();
    typeString = "PETSCII";
  }
  //}}}

  //{{{ encASCII()
  /**
   *  Encodes the string as ASCII
   *
   * @param  text  The string to encode
   */
  public void encASCII( String text )
  {
    textString = text;
    numBytes = textString.length();
    typeString = "ANSI";
  }
  //}}}

  //{{{ encScreen()
  /**
   *  Encodes the string as screen codes
   *
   * @param  text  The string to encode
   */
  public void encScreen( String text )
  {
    textString = asciiToScreen( text );
    numBytes = textString.length();
    typeString = "SCREEN";
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    Vector code = new Vector();

    for( int i = 0; i < textString.length(); i++ ) {
      code.add( new Integer( (int)textString.charAt( i ) ) );
    }

    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this StringIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "StringIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes + ", " + typeString;
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "StringIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "Type: " + typeString + ", size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}

  //{{{ asciiToPetscii()
  /**
   *  Converts an ASCII string to PETSCII.
   *
   * @param  text  The string to convert
   * @return       The converted string
   */
  private String asciiToPetscii( String text )
  {
    StringBuffer strBuf = new StringBuffer();

    for( int i = 0; i < text.length(); i++ ) {
      int val = (int)text.charAt( i );

      val &= 0x7f;
      if( val >= 'A' && val <= 'Z' ) {
        val += 32;
      }
      else if( val >= 'a' && val <= 'z' ) {
        val -= 32;
      }
      strBuf.append( (char)val );
    }

    return strBuf.toString();
  }
  //}}}

  //{{{ asciiToScreen
  /**
   *  Converts an ASCII string to screen codes.
   *
   * @param  text  The string to convert
   * @return       The converted string
   */
  private String asciiToScreen( String text )
  {
    StringBuffer strBuf = new StringBuffer();

    for( int i = 0; i < text.length(); i++ ) {
      int val = (int)text.charAt( i );

      val &= 0x7f;
      if( val >= 'A' && val <= 'Z' ) {
        val -= 64;
      }
      else if( val >= 'a' && val <= 'z' ) {
        val -= 96;
      }
      else if( val == 64 ) {
        val = 0;
      }
      else if( val == 96 ) {
        val = 0;
      }
      strBuf.append( (char)val );
    }

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

