/*
 *  FloatConstant.java - Represents a float constant
 *
 *  Copyright (C) 2003-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 *  represents a float constant
 *
 * @author     andreas
 * @created    12 October 2003
 */
public class FloatConstant extends Expression
{
  //{{{ Private objects
  private float value;
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the FloatConstant object
   */
  public FloatConstant()
  {
    value = 0;
  }
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the FloatConstant object
   *
   * @param  val  The value
   */
  public FloatConstant( float val )
  {
    value = val;
  }
//}}}

  //{{{ getValue()
  /**
   *  Returns the value
   *
   * @return    The value
   */
  public int getValue()
  {
    return (int)value;
  }
  //}}}

  //{{{ toString
  /**
   *  Prints information about this expression.
   *
   * @return    The string representation of the expression
   */
  public String toString()
  {
    String out = "";

    out += "FloatConstant ($" + Float.toHexString( value ).toUpperCase() + ")";

    return out;
  }
  //}}}

}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

