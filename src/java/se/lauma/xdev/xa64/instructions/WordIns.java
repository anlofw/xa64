/*
 *  WordIns.java - Represents a .word instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

//{{{ Imports
import java.util.*;
import se.lauma.xdev.xa64.*;
//}}}

/**
 *  Represents a .word instruction
 *
 * @author     Andreas Lofwenmark
 * @created    February 27, 2005
 */
public class WordIns extends Ins
{
  //{{{ Private objects
  private Vector words = new Vector();
  //}}}

  //{{{ Constructor()
  /**
   *  Constructor for the WordIns object.
   *
   * @param  line  Source code line associated with this instruction
   */
  public WordIns( int line )
  {
    super( line );
    numBytes = 0;
  }
  //}}}

  //{{{ add()
  /**
   *  Adds a new word.
   *
   * @param  word  The word to be added
   */
  public void add( Expression word )
  {
    words.add( word );
    numBytes += 2;
  }
  //}}}

  //{{{ generateCode()
  /**
   *  Generates the object code for the .word instruction.
   *
   * @return    The generated object code
   */
  public Vector generateCode()
  {
    Vector code = new Vector();

    try {
      for( int i = 0; i < words.size(); i++ ) {
        int val = ( (Expression)words.elementAt( i ) ).getValue();

        if( val >= -0xffff && val <= 0xffff ) {
          code.add( new Integer( val & 0xff ) );
          code.add( new Integer( val / 0x100 ) );
        }
        else {
          ErrorLog.error( "line " + line +
            ": Word at index " + ( i + 1 ) + " out of range" );
        }
      }
    }
    catch( NumberFormatException exception ) {
      String msg = exception.getMessage();
      int index = msg.indexOf( '.' );
      String label = msg;
      String scope = "";

      if( index > 0 ) {
        label = msg.substring( index );
        scope = " in the scope of " + msg.substring( 0, index );
      }
      ErrorLog.error( "line " + line + ": "
         + label + " is undefined" + scope );
      code = null;
    }

    return code;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this WordIns.
   *
   * @return    The string representation of the instruction
   */
  public String toString()
  {
    return "WordIns: address = $" + Integer.toHexString( address ).toUpperCase() +
      ", size = " + numBytes;
  }
  //}}}

  //{{{ astToString()
  /**
   *  Prints the AST for this instruction.
   *
   * @return    The string representation of the AST for this instruction
   */
  public String astToString()
  {
    StringBuffer strBuf = new StringBuffer();

    strBuf.append( "WordIns\n" );
    strBuf.append( lastChild() );
    strBuf.append( "Size: " + numBytes );
    endChild();

    return strBuf.toString();
  }
  //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

