/*
 *  Gt.java - Represents the high byte of an expression
 *
 *  Copyright (C) 2003-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 *  Description of the Class
 *
 * @author     andreas
 * @created    12 October 2003
 */
public class Gt extends UnaryOp
{
  //{{{ Constructor()
  /**
   *  Constructor for the Gt object
   *
   * @param  e  The expression to get the high byte from
   */
  public Gt( Expression e )
  {
    right = e;
  }
  //}}}

  //{{{ getValue()
  /**
   *  Returns the high byte of the expression
   *
   * @return    The value
   */
  public int getValue()
  {
    return right.getValue() / 256;
  }
  //}}}

  //{{{ toString()
  /**
   *  Prints information about this expression.
   *
   * @return    The string representation of the expression
   */
  public String toString()
  {
    String out = "";

    out += "High byte (>) of\n";
    out += lastChild();
    out += right;
    endChild();

    return out;
  } //}}}
}

// :tabSize=8:indentSize=2:noTabs=true:
// :folding=explicit:collapseFolds=1:

