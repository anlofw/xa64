//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/AstNode.java $
// $LastChangedRevision: 1 $
// $LastChangedDate: 2006-08-23 01:36:11 +0200 (Wed, 23 Aug 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 *  Description of the Class
 *
 * @author     andreas
 * @created    July 17, 2005
 */
public abstract class AstNode
{
  /**
   *  Description of the Field
   */
  protected static int indentLevel = 0;
  /**
   *  Description of the Field
   */
  protected static boolean[] branches = new boolean[10000];

  private String out;

  /**
   *  Constructor for the AstNode object
   */
  public AstNode()
  {
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  public String toString()
  {
    return "AstNode";
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  protected String indent()
  {
    out = "";
    for( int i = 0; i < indentLevel; i++ ) {
      if( branches[i] ) {
        out += "|";
      }
      else {
        out += " ";
      }
    }
    return out;
  }

  /**
   *  Description of the Method
   */
  protected void indentMore()
  {
    indentLevel += 2;
  }

  /**
   *  Description of the Method
   */
  protected void indentLess()
  {
    indentLevel -= 2;
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  protected String beginChild()
  {
    out = "";
    out += indent();
    branches[indentLevel] = true;
    indentMore();

    return out + "+-";
  }

  /**
   *  Description of the Method
   */
  protected void endChild()
  {
    indentLess();
    if( branches[indentLevel] ) {
      branches[indentLevel] = false;
    }
    else {
    }
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  protected String lastChild()
  {
    out = "";
    out += indent();
    branches[indentLevel] = false;
    indentMore();

    return out + "+-";
  }

}

