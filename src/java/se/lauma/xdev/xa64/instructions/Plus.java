//
// $HeadURL: http://www.lauma.se/svn/xdev/trunk/xa64/src/java/se/lauma/xdev/xa64/instructions/Plus.java $
// $LastChangedRevision: 8 $
// $LastChangedDate: 2006-10-08 02:57:30 +0200 (Sun, 08 Oct 2006) $
// $LastChangedBy: Andreas $
//

package se.lauma.xdev.xa64.instructions;

/**
 * The class Plus is used to represent an addition
 *
 * @author  Andreas Lofwenmark
 */

public class Plus extends BinaryOp
{
  public Plus( Expression l, Expression r )
  {
    left = l;
    right = r;
  }

  public int getValue()
  {
    return left.getValue() + right.getValue();
  }

  public String toString()
  {
    return toString("Plus");
  }
}

// set emacs indentation
// Local Variables:
// c-basic-offset:2
// End:
