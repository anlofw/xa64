/*
 *  Division.java - Represents a division operation
 *
 *  Copyright (C) 2005-2006 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 *  The class Division is used to represent a division
 *
 * @author     Andreas Lofwenmark
 * @created    22 November 2003
 */

public class Division extends BinaryOp
{
  /**
   *  Constructor for the Division object
   *
   * @param  l  Description of the Parameter
   * @param  r  Description of the Parameter
   */
  public Division( Expression l, Expression r )
  {
    left = l;
    right = r;
  }

  /**
   *  Gets the value attribute of the Division object
   *
   * @return    The value value
   */
  public int getValue()
  {
    return left.getValue() / right.getValue();
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  public String toString()
  {
    return toString( "Division" );
  }
}

