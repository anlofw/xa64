/*
 *  Not.java - Represents a .byte instruction
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package se.lauma.xdev.xa64.instructions;

/**
 *  The class Not is used to represent !expression
 *
 * @author     Andreas Löfwenmark
 * @created    19 July 2005
 */

public class Not extends UnaryOp
{
  /**
   *  Constructor for the Not object
   *
   * @param  e  Description of the Parameter
   */
  public Not( Expression e )
  {
    right = e;
  }

  /**
   *  Gets the value attribute of the Not object
   *
   * @return    The value value
   */
  public int getValue()
  {
    return ~right.getValue();
  }

  /**
   *  Description of the Method
   *
   * @return    Description of the Return Value
   */
  public String toString()
  {
    String out = "";

    out += "Inverse (1's complement) of\n";
    out += lastChild();
    out += right;
    endChild();

    return out;
  }

}

