/*
 *  ErrorLog.java - Handles the error and warning counting and output
 *
 *  Copyright (C) 2005 Andreas Lofwenmark
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.package se.lauma.xdev.xa64;
 */
package se.lauma.xdev.xa64;

import java.util.Stack;

/**
 *  Handles the error and warning counting and output.
 *
 * @author     Andreas Lofwenmark
 * @created    February 27, 2005
 */
public class ErrorLog
{
  private static int numErrors = 0;
  private static int numWarnings = 0;
  private static boolean verbose = false;
  private static Stack filenames = new Stack();

  /**
   *  Prints an error message and increases the error counter.
   *
   * @param  error  The error message to print
   */
  public static void error( String error )
  {
    System.err.println( "Error: " + (String)filenames.peek() + " - " + error );
    numErrors += 1;
  }

  /**
   *  Prints a warning message and increases the warning counter.
   *
   * @param  warning  The warning message to print
   */
  public static void warning( String warning )
  {
    if( !filenames.empty() ) {
      System.err.println( "Warning: " + (String)filenames.peek() + " - " + warning );
    }
    else {
      System.err.println( "Warning: " + warning );
    }
    numWarnings += 1;
  }

  /**
   *  Prints a fatal error message and exits the application.
   *
   * @param  msg  The fatal error message to print
   */
  public static void fatal( String msg )
  {
    System.err.println( "Fatal Error: " + msg );
    System.exit( 1 );
  }

  /**
   *  Prints a fatal error message, but doesn't exit the application.
   *
   * @param  msg  The fatal error message to print
   */
  public static void fatalNoExit( String msg )
  {
    System.err.println( "Fatal Error: " + msg );
  }

  /**
   *  Prints a message if verbose mode is on.
   *
   * @param  msg  The message to print
   */
  public static void message( String msg )
  {
    if( verbose ) {
      System.out.println( "Message: " + (String)filenames.peek() + " - " + msg );
    }
  }

  /**
   *  Resets the error and warning counters.
   */
  public static void reset()
  {
    numErrors = 0;
    numWarnings = 0;
  }

  /**
   *  Sets the verbose level.
   *
   * @param  verbose  The new verbose value
   */
  public static void setVerbose( boolean verbose )
  {
    ErrorLog.verbose = verbose;
  }

  /**
   *  Gets the number of errors.
   *
   * @return    The numErrors value
   */
  public static int getNumErrors()
  {
    return numErrors;
  }

  /**
   *  Gets the number of warnings.
   *
   * @return    The numWarnings value
   */
  public static int getNumWarnings()
  {
    return numWarnings;
  }

  /**
   *  Changes the file to associate the messages with
   *
   * @param  filename  Description of the Parameter
   */
  public static void startFile( String filename )
  {
    filenames.push( filename );
  }

  /**
   *  Reverts to the previous file
   */
  public static void endFile()
  {
    filenames.pop();
  }
}

