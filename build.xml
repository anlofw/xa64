<?xml version="1.0"?>

<!--
   =======================
     Build file for xa64
   =======================
-->  

<project name="xa64" default="dist" basedir=".">
  <description>
    Build xa64, the C64 cross assembler part of X-Developer
  </description>

  <property file="../xdev.properties" />
  <property file="xa64.properties" />
  
  <property name="src.dir" location="${basedir}/src" />
  <property name="src.lib" location="${basedir}/src/lib" />
  <property name="src.etc" location="${basedir}/src/etc" />

  <property name="build.base" value="build" />
  <property name="build.dir" value="${build.base}" />
  <property name="build.src" value="${build.dir}/src" />
  <property name="build.classes" value="${build.dir}/classes" />
  <property name="build.lib" value="${build.dir}/lib" />
  <property name="build.etc" value="${build.dir}/etc" />
  <property name="build.docs" value="${build.dir}/docs" />
  <property name="build.javadocs" value="${build.docs}/api" />
  
  <property name="lexer.src.dir" location="${src.dir}/java/se/lauma/xdev/xa64" />
  <property name="parser.src.dir" location="${lexer.src.dir}" />

  <property name="flex.asm.java" location="${build.src}/se/lauma/xdev/xa64/Lexer.java" />
  <property name="cup.asm.java" location="${build.src}/se/lauma/xdev/xa64/Parser.java" />

  <property name="flex_name" value="xa64.flex" />
  <property name="cup_name" value="xa64.cup" />
  
  <fileset id="assembler.src" dir="${src.dir}/java">
    <include name="se/**/*.java" />
    <include name="se/**/*.properties" />
    <include name="se/**/*.flex" />
    <include name="se/**/*.cup" />
  </fileset>

  <fileset id="build.src" dir="${build.src}">
    <include name="se/**/*.java" />
  </fileset>

  <fileset id="build.classes" dir="${build.classes}">
    <include name="**/*.class" />
    <exclude name="java_cup/**/*.class" />
  </fileset>
  
  <fileset id="flex.src" dir="${src.dir}/java/se/lauma/xdev/xa64">
    <include name="${flex_name}" />
  </fileset>

  <fileset id="flex.build" dir="${build.src}/se/lauma/xdev/xa64">
    <include name="${flex_name}" />
  </fileset>
  
  <fileset id="flex.java" dir="${build.src}/se/lauma/xdev/xa64">
    <include name="Lexer.java" />
  </fileset>

  <fileset id="cup.src" dir="${src.dir}/java/se/lauma/xdev/xa64">
    <include name="${cup_name}" />
  </fileset>

  <fileset id="cup.java" dir="${build.src}/se/lauma/xdev/xa64">
    <include name="Parser.java" />
    <include name="Sym.java" />
  </fileset>

  <fileset id="build.number" dir=".">
    <include name="build.number" />
  </fileset>

  <fileset id="main.java" dir="${main.dir}">
    <include name="Xa64.java" />
  </fileset>
  
  <target name="init">
    <tstamp>
      <format property="year" pattern="yyyy" />
    </tstamp>
    
    <path id="classpath">
      <pathelement location="${java_cup.jar}" />
      <pathelement location="${commons-cli.jar}" />
    </path>
    
    <mkdir dir="${build.dir}" />
    <mkdir dir="${build.src}" />
    <mkdir dir="${build.classes}" />
    <mkdir dir="${build.lib}" />
    <mkdir dir="${build.etc}" />
    
    <copy toDir="${build.src}" includeEmptyDirs="no" filtering="off">
      <fileset refid="assembler.src" />
    </copy>
    
    <copy toDir="${build.classes}">
      <fileset dir="${src.lib}">
        <patternset>
	  <include name="java_cup/runtime/*.class" />
	</patternset>
      </fileset>
    </copy>

    <copy toDir="${build.classes}">
      <fileset dir="${build.src}">
        <patternset>
	  <include name="se/lauma/xdev/xa64/instructions/*.properties" />
	</patternset>
      </fileset>
    </copy>
    
    <copy toDir="${build.etc}">
      <fileset dir="${src.etc}">
        <patternset>
          <include name="xa64.manifest" />
        </patternset>
      </fileset>
    </copy>
  </target>    

  <target name="lexer-deps" depends="init">
    <dependset>
      <srcfileset refid="flex.build" />
      <targetfileset refid="flex.java" />
    </dependset>
    <available property="lexer.up-to-date" file="${flex.asm.java}" />
  </target>

  <target name="lexer" depends="init, lexer-deps"
                       unless="lexer.up-to-date">
    <java classname="JFlex.Main"
          classpath="${jflex.jar}"
	  fork="true"
	  dir="${build.src}/se/lauma/xdev/xa64">
	  <arg line="${flex_name}" />
    </java>
<!--    <move file="${build.src}/lexer_parser/Lexer.java" todir="${build.src}/assembler/"/>
-->
    
  </target>

  <target name="parser-deps" depends="init">
    <dependset>
      <srcfileset refid="cup.src" />
      <targetfileset refid="cup.java" />
    </dependset>
    <available property="parser.up-to-date" file="${cup.asm.java}" />
  </target>

  <target name="parser" depends="lexer,parser-deps"
                        unless="parser.up-to-date">
    <java classname="java_cup.Main"
          classpath="${java_cup.jar}"
	  fork="true"
	  dir="${build.src}/se/lauma/xdev/xa64">
	  <arg line="-parser Parser -expect 4 -symbols Sym -nopositions -package se.lauma.xdev.xa64 ${cup_name}" />
    </java>
 
  </target>

  <target name="xa64-deps" depends="init">
    <dependset>
      <srcfileset refid="build.src" />
      <targetfileset refid="build.classes" />
    </dependset>
    <available property="xa64.up-to-date" file="${build.classes}/se/lauma/xdev/xa64/Xa64.class" />
  </target>

  <target name="xa64" depends="init,parser,xa64-deps"
                      unless="xa64.up-to-date"
                      description="- Build Xa64">
    <buildnumber />
    <copy tofile="${build.src}/se/lauma/xdev/xa64/Version.java" overwrite="yes">
      <fileset dir="${src.dir}" includes="java/se/lauma/xdev/xa64/Version.java" />
    </copy>
    <replace file="${build.src}/se/lauma/xdev/xa64/Version.java" token="@VERSION@" value="${version}"/>    
    <replace file="${build.src}/se/lauma/xdev/xa64/Version.java" token="@BUILD@" value="${build.number}"/>    
    <javac destdir="${build.classes}" source="1.4" target="1.4">
      <src>
        <pathelement location="${build.src}/se/lauma/xdev/xa64"></pathelement>
      </src>
      <classpath refid="classpath" />
    </javac>
  </target>

  <target name="dist"
          depends="xa64"
	  description="- Create Xa64 jar file">
    <jar jarfile="${build.lib}/xa64.jar"
         manifest="${build.etc}/xa64.manifest"
	 basedir="${build.classes}" />
  </target>
  
  <target name="clean"
          description="- Clean">
    <delete dir="${build.base}"/>
  </target>

</project>

